package com.kagou.mybuttomtab;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kagou.mybuttomtab.ykqui.LetterSpacingTextView;
import com.kagou.mybuttomtab.ykqui.OnCheckedChangeListener;
import com.kagou.mybuttomtab.ykqui.YkqButtomTab;

public class MainActivity extends AppCompatActivity {

    private LetterSpacingTextView textView;
    private YkqButtomTab buttomTab;
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (LetterSpacingTextView)findViewById(R.id.tv_01);
        textView.setLetterSpacing(30);

        buttomTab = findViewById(R.id.buttom_tab);
        buttomTab.setSelectedAt(3);
        buttomTab.setChildTextSizeForPx(40);

        Log.d(TAG, "onCreate: " + R.id.one + "--" + R.id.two + "--" + R.id.three + "--" + R.id.four);
        buttomTab.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(int id) {
                Log.d(TAG, "onCheckedChanged: viewId" + id);
            }
        });

    }
}