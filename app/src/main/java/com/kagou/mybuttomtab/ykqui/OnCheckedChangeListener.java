package com.kagou.mybuttomtab.ykqui;

public interface OnCheckedChangeListener {
    void onCheckedChanged(int id);
}
