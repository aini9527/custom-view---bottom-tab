package com.kagou.mybuttomtab.ykqui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.kagou.mybuttomtab.R;
import com.kagou.mybuttomtab.util.AppUtil;

public class YkqButtomTab extends ViewGroup {
    private static final String TAG = "YkqButtomTab";

    private int ykqButtomTabBackGround = 0;
    private int selectedIndex = -1;
    private float childTextSizeForSp;
    private float childTextSizeForPx;

    private OnCheckedChangeListener onCheckedChangeListener;

    public YkqButtomTab(Context context) {
        super(context);
    }

    public YkqButtomTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        //使得ViewGroup执行onDraw方法；默认是不执行的。
        super.setWillNotDraw(false);
        init(context, attrs);
    }
    public YkqButtomTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //使得ViewGroup执行onDraw方法；默认是不执行的。
        super.setWillNotDraw(false);

        init(context, attrs);

    }

    private void  init(Context context, AttributeSet attrs){
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.YkqButtomTab);
        this.ykqButtomTabBackGround = array.getColor(R.styleable.YkqButtomTab_ykqButtomTabbackground, Color.WHITE);
        childTextSizeForSp = array.getDimension(R.styleable.YkqButtomTab_ykqButtomTabTextSize, 0);
        childTextSizeForPx = AppUtil.sp2px(context, childTextSizeForSp);
        array.recycle();
        initData();
        initChild();
    }

    private void initData(){
        setBackgroundColor(ykqButtomTabBackGround);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int childCount = getChildCount();
        int width = 0;
        int height = 0;

        for(int i = 0; i < childCount; i++){
            View child = getChildAt(i);
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            width += child.getMeasuredWidth();
            height =Math.max(height, child.getMeasuredHeight());
        }

        Log.d(TAG, "onMeasure: widthMode=" + widthMode + "--" + "widthSize=" + widthSize + "--" + "width=" + width + "--" + "heightMode=" + heightMode
         + "--" + "heightSize=" + heightSize + "--" + "height=" + height);
        setMeasuredDimension((widthMode == MeasureSpec.EXACTLY ? widthSize : width), (heightMode == MeasureSpec.EXACTLY ? heightSize : height));
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {
        int childCount = getChildCount();
        int thisWidth = getMeasuredWidth();
        int thisHeight = getMeasuredHeight();

        int preChildSize = thisWidth / childCount;

        int point = 0;
//        for(int index = 0; index < childCount; index++){
//            View child = getChildAt(index);
//            point = point + (preChildSize - getChildAt(index).getMeasuredWidth()) / 2;
//            child.layout(point, 0, point + getChildAt(index).getMeasuredWidth(), thisHeight);
//            Log.d(TAG, "onLayout: child" + index + "宽" + child.getWidth() + "child" + index + "高" + child.getHeight());
//            point = preChildSize*(index + 1);
//        }
        for(int index = 0; index < childCount; index++){
            View child = getChildAt(index);
            child.layout(point, 0, point + preChildSize - 10, thisHeight);
            Log.d(TAG, "onLayout: child" + index + "宽" + child.getWidth() + "child" + index + "高" + child.getHeight());
            point += preChildSize;
        }

        initChild();
        addListen();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int childCount = getChildCount();
        int thisWidth = getMeasuredWidth();
        int thisHeight = getMeasuredHeight();

        int preChildSize = thisWidth / childCount;
        int draw_x = preChildSize - 2;

        Paint paint = new Paint();
        paint.setStrokeWidth(2);
        paint.setColor(getResources().getColor(R.color.bottom_tab_split));
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        for(int j = 1; j < childCount; j++){
            canvas.drawLine(draw_x, 0, draw_x, thisHeight, paint);
            draw_x += preChildSize - 2;
        }

        paint.setStrokeWidth(5);
        canvas.drawLine(0, 0, thisWidth, 0, paint);

    }

    private void initChild(){
        setViewIndex(getChildCount());

        if(childTextSizeForSp != 0){
            for(int i = 0; i < getChildCount(); i++){
                ((YkqSubButtonTab)getChildAt(i)).setTextSizeForSp(childTextSizeForSp);
            }
        }

    }

    private void setViewIndex(int childCount){
        for(int i = 0; i < childCount; i++){
            ((YkqSubButtonTab)getChildAt(i)).setIndex(i);
        }
    }

    private void addListen(){
        final int childCount = getChildCount();

        for(int i = 0; i < childCount; i++){
            ((YkqSubButtonTab)getChildAt(i)).setChildSelected(new ChildSelected() {
                @Override
                public void onSelected(View view) {
                    if(selectedIndex > -1 && selectedIndex < childCount){
                        ((YkqSubButtonTab)getChildAt(selectedIndex)).setSelected(false);
                        getChildAt(selectedIndex).invalidate();

                        ((YkqSubButtonTab)view).setSelected(true);
                        view.invalidate();

                        selectedIndex = ((YkqSubButtonTab)view).getIndex();

                        if(getOnCheckedChangeListener() != null){
                            getOnCheckedChangeListener().onCheckedChanged(view.getId());
                        }
                    }
                }
            });
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return true;
    }

    public void setTextColorNormal(int textColorNormal){
        int childCount = getChildCount();

        for(int i = 0; i < childCount; i++){
            ((YkqSubButtonTab)getChildAt(i)).setTextColorNormal(textColorNormal);
        }
    }

    public void setTextColorSelected(int textColorSelected){
        int childCount = getChildCount();

        for(int i = 0; i < childCount; i++){
            ((YkqSubButtonTab)getChildAt(i)).setTextColorSelected(textColorSelected);
        }
    }

    public int getSelectedIndex(){
        return this.selectedIndex;
    }

    public void setSelectedAt(int index){
        int childCount = getChildCount();
        if(index > (childCount - 1) && index < 0){
            return;
        }
        if(selectedIndex == -1){
            ((YkqSubButtonTab)getChildAt(index)).setSelected(true);
            getChildAt(index).invalidate();
            selectedIndex = index;
        }else{
            ((YkqSubButtonTab)getChildAt(index)).setSelected(true);
            getChildAt(index).invalidate();
            ((YkqSubButtonTab)getChildAt(selectedIndex)).setSelected(false);
            getChildAt(selectedIndex).invalidate();
            selectedIndex = index;
        }
    }

    public float getChildTextSizeForPx() {
        return childTextSizeForPx;
    }

    public void setChildTextSizeForPx(int childTextSize) {
        this.childTextSizeForPx = childTextSize;
        int childCount = getChildCount();
        for(int i = 0; i < childCount; i++){
            ((YkqSubButtonTab)getChildAt(i)).setTextSize(childTextSize);
        }
    }

    public OnCheckedChangeListener getOnCheckedChangeListener() {
        return onCheckedChangeListener;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        this.onCheckedChangeListener = onCheckedChangeListener;
    }
}

