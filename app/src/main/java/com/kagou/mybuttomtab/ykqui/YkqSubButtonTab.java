package com.kagou.mybuttomtab.ykqui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.kagou.mybuttomtab.R;
import com.kagou.mybuttomtab.util.AppUtil;

public class YkqSubButtonTab extends ViewGroup {

    private int imageNormalId;
    private int imageSelectedId;
    private String tabText;
    private Context context = null;

    private boolean isSelected = false;
    private ChildSelected childSelected;

    private Paint textPaint;
    private int textColorNormal;
    private int textColorSelected;
    private int index;
    private float textSize;
    private static final String TAG = "YkqSubButtonTab";
    public YkqSubButtonTab(Context context) {
        super(context);
        this.context = context;
    }

    public YkqSubButtonTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        super.setWillNotDraw(false);
        this.context = context;

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.YkqSubBottomTab);
        imageNormalId = array.getResourceId(R.styleable.YkqSubBottomTab_ykqSubBottomTabImage_normal, 0);
        imageSelectedId = array.getResourceId(R.styleable.YkqSubBottomTab_ykqSubBottomTabImage_selected, 0);
        tabText = array.getString(R.styleable.YkqSubBottomTab_ykqSubButtomTabText);
        textSize = AppUtil.sp2px(context, getResources().getDimension(R.dimen.bottom_tab_textSize_default));
        textColorNormal = array.getColor(R.styleable.YkqSubBottomTab_ykqSubBottomTab_textcolor_normal, Color.BLACK);
        textColorSelected = array.getColor(R.styleable.YkqSubBottomTab_ykqSubBottomTab_textcolor_selected, Color.GREEN);
        array.recycle();
        addListen();

        initTextPaint();
    }

    public YkqSubButtonTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        super.setWillNotDraw(false);
        this.context = context;
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initTextPaint(){
        textPaint = new Paint();
        textPaint.setTextSize(textSize);
        textPaint.setColor(textColorNormal);
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setStrokeWidth(0.5F);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int thisWidth = getWidth();
        int thisHeight = getHeight();
        Paint.FontMetricsInt fontMetricsInt = textPaint.getFontMetricsInt();
        int textHeight = fontMetricsInt.bottom - fontMetricsInt.top;
        super.onDraw(canvas);

        if(tabText == null || "".equals(tabText)){
            if(!isSelected){
                if(imageNormalId != 0){
                    Bitmap bitmap = AppUtil.getBitmapFromVectorDrawable(context, imageNormalId);
                    bitmap = Bitmap.createScaledBitmap(bitmap, thisHeight-textHeight, thisHeight-textHeight, true);
                    Paint paint = new Paint();
                    canvas.drawBitmap(bitmap, (thisWidth - (thisHeight-textHeight)) / 2, 0, paint);
                }
            }else{
                if(imageSelectedId != 0){
                    Bitmap bitmap = AppUtil.getBitmapFromVectorDrawable(context, imageSelectedId);
                    bitmap = Bitmap.createScaledBitmap(bitmap, thisHeight-textHeight, thisHeight-textHeight, true);
                    Paint paint = new Paint();
                    canvas.drawBitmap(bitmap, (thisWidth - (thisHeight-textHeight)) / 2, 0, paint);
                }
            }
        }else{
            if(!isSelected){
                if(imageNormalId != 0){
                    Bitmap bitmap = AppUtil.getBitmapFromVectorDrawable(context, imageNormalId);
                    bitmap = Bitmap.createScaledBitmap(bitmap, thisHeight-textHeight, thisHeight-textHeight, true);
                    Paint paint = new Paint();
                    canvas.drawBitmap(bitmap, (thisWidth - (thisHeight-textHeight)) / 2, 0, paint);
                }
                setTextColor(textColorNormal);
            }else{
                if(imageSelectedId != 0){
                    Bitmap bitmap = AppUtil.getBitmapFromVectorDrawable(context, imageSelectedId);
                    bitmap = Bitmap.createScaledBitmap(bitmap, thisHeight-textHeight, thisHeight-textHeight, true);
                    Paint paint = new Paint();
                    canvas.drawBitmap(bitmap, (thisWidth - (thisHeight-textHeight)) / 2, 0, paint);
                }
                setTextColor(textColorSelected);
            }

            if(textPaint != null){
                int textWidth = (int)textPaint.measureText(tabText);
                int startText = (thisWidth - textWidth) / 2;
                canvas.drawText(tabText, startText, thisHeight-textHeight-fontMetricsInt.ascent, textPaint);
            }

        }

    }

    private void addListen(){
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getChildSelected() != null){
                    Log.d(TAG, "onClick: item点击");
                    getChildSelected().onSelected(YkqSubButtonTab.this);
                }

            }
        });
    }

    public int getTextColorNormal() {
        return textColorNormal;
    }

    protected void setTextColorNormal(int textColorNormal) {
        this.textColorNormal = textColorNormal;
    }

    public int getTextColorSelected() {
        return textColorSelected;
    }

    protected void setTextColorSelected(int textColorSelected) {
        this.textColorSelected = textColorSelected;
    }

    private void setTextColor(int textColor){
        textPaint.setColor(textColor);
    }

    public String getTabText() {
        return tabText;
    }

    public void setTabText(String tabText) {
        this.tabText = tabText;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ChildSelected getChildSelected() {
        return childSelected;
    }

    public void setChildSelected(ChildSelected childSelected) {
        this.childSelected = childSelected;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        Log.d(TAG, "setTextSize: textSize" + this.textSize);
        textPaint.setTextSize(this.textSize);
    }
    public void setTextSizeForSp(float textSize) {
        this.textSize = AppUtil.sp2px(context, textSize);
        Log.d(TAG, "setTextSizeForSp: textSize" + this.textSize);
        textPaint.setTextSize(this.textSize);
    }
}

interface ChildSelected{
    void onSelected(View view);
}
